"use strict"
var bitcore = require('bitcore-node');
var lib = require('bitcore-lib');
var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var async = require('async');
var stats = require("stats-lite");
var config = require('./libs/config');
var bufferUtil = lib.util.buffer;


//Services
var Bitcoin = bitcore.services.Bitcoin;
// rio.e({command: "source('/home/ruser/Gap600/GAP600.R')"});
//     rio.e({
//         filename:'/home/ruser/Gap600/GAP600.R',
//         entrypoint: "echo",
//         data: "hello world",
//         callback: printEcho
//     });
// function printEcho(err, res) {
//     if (err) {
//         console.log("An error occured : " + err);
//     } else {
//         console.log("Response is : " + res);
//     }
// }

var configuration = {
    network: 'livenet',
    services: [
        {
            name: 'bitcoind',
            module: Bitcoin,
            config: {
                spawn: {
                    "datadir": "/root/.bitcore/data",
                    "exec": "/root/.nvm/versions/node/v0.12.10/lib/node_modules/bitcore/node_modules/bitcore-node/bin/bitcoind"
                }
            }
        }
    ]
}

var node = new bitcore.Node(configuration);

node.start(function (err) {
    if (err) {
        assert.equal(null, err)
        node.stop(function () {
            console.log('shutdown the node');
        });
    }
});

node.on('error', function (err) {
    assert.equal(null, err);
    node.stop();
});

node.on('ready', function () {
    async.parallel({
            RE5: connectToDb.bind(null, config.riskEngineDB),
            MDB: connectToDb.bind(null, config.testDB),
            RE1: connectToDb.bind(null, config.replicaSetDB)
        },
        function (err, DBs) {
            if (DBs.Local === err || DBs.AM2 === err || DBs.RE === err) {
                throw  new Error("dose`t connect to Dbs")
            }
            try {
                console.log("Connected correctly to Dbs");
                var testCol = DBs.RE5.collection('testCol');
                var userAccountCol = DBs.RE5.collection('userAccountCol');
                var mainColl = DBs.RE1.collection('transactions_replica');
                // mainColl.find({
                //     $and: [
                //         { blockHash: { $exists: true } },
                //         { userAccount: { $exists: true } }
                //     ] }, {hash:true,'txStaticInfo.inputs':true}).toArray(function (err, docs) {
                //     if(err) console.log(err);
                //     console.log(docs.length)
                //     docs.forEach(function (trx) {
                //         console.log(trx.txStaticInfo.inputs.length)
                //         trx.txStaticInfo.inputs.forEach(function (input) {
                //             console.log(input.prevTxId);
                //         })
                //     })
                // })

                // var reTwoCol = DBs.RE2.collection('transactions_re2');
                // var blocksCol = DBs.MDB.collection('updatedBlocks');
                // var blockDetailsCol = DBs.MDB.collection(con
                node.services.bitcoind.getInfo(function (err, info) {
                    if (err) throw err;
                    var blockNumber = info.blocks;
                    node.services.bitcoind.getBlock(blockNumber, function (err, blocks) {
                        if (err) console.log(err);
                        updateBlockList(blocks, testCol, function (err, res) {
                            if (err) console.log(err);
                            console.log(res);
                            mainColl.find({
                                $and: [
                                    {blockHash: {$exists: false}},
                                    {userAccount: {$exists: true}},
                                    {riskScore: 1}
                                ]
                            }, {
                                hash: true,
                                userAccount: true,
                                riskScore: true,
                                'txStaticInfo.inputs': true
                            }).toArray(function (err, docs) {
                                if (err) console.log(err);
                                console.log("docs.length", docs.length)
                                docs.forEach(function (trx) {
                                    console.log("trx:", trx);
                                    trx.txStaticInfo.inputs.forEach(function (input) {
                                        //console.log(input.prevTxId);
                                        testCol.findOne({
                                            $and: [
                                                {prevTxId: input.prevTxId},
                                                {outputIndex: input.outputIndex}
                                            ]
                                        }, function (err, doc) {
                                            if (doc && trx.hash != doc.hash) {
                                                console.log('ok');
                                                userAccountCol.updateOne({hash: trx.hash}, {
                                                    $set: {
                                                        userAccount: trx.userAccount,
                                                        hash2: doc.hash
                                                    }
                                                }, {upsert: true});
                                            }
                                        });
                                    })
                                })
                            })
                        })
                    });

                    node.services.bitcoind.on('block', function (blockHash) {
                        console.log(new Date(), blockHash);
                        setTimeout(function(){
                            var blockHashHax = bufferUtil.bufferToHex(blockHash);
                            node.services.bitcoind.getBlock(blockHashHax, function (err, blocks) {
                                if (err) console.log(err);
                                updateBlockList(blocks, testCol, function (err, res) {
                                    if (err) console.log(err);
                                    console.log(res);
                                    mainColl.find({
                                        $and: [
                                            { blockHash: { $exists: false } },
                                            { userAccount: { $exists: true } },
                                            { riskScore: 1}
                                        ] }, {hash:true, userAccount:true,  riskScore: true, 'txStaticInfo.inputs':true }).toArray(function (err, docs) {
                                        if(err) console.log(err);
                                        console.log("docs.length", docs.length)
                                        docs.forEach(function (trx) {
                                            console.log("trx:",trx);
                                            trx.txStaticInfo.inputs.forEach(function (input) {
                                                console.log(input.prevTxId);
                                                testCol.findOne({
                                                    $and: [
                                                        {prevTxId: input.prevTxId},
                                                        {outputIndex: input.outputIndex}
                                                    ]
                                                }, function (err, doc) {
                                                    if(doc && trx.hash != doc.hash){
                                                        console.log('ok');
                                                        var json = {
                                                            hash: trx.hash,
                                                            userAccount: trx.userAccount
                                                        }
                                                        console.log(json);
                                                        userAccountCol.updateOne({hash: trx.hash },{$set :{userAccount: trx.userAccount, hash2: doc.hash} },{upsert:true});
                                                    }
                                                });
                                            })
                                        })
                                    })
                                })
                            });
                        },3000);

                    })

                    // node.services.bitcoind.getRawBlock(blockNumber, function(err, blockBuffer) {
                    //     if (err) {
                    //         throw err;
                    //     }
                    //
                    //     var block = lib.Block.fromBuffer(blockBuffer)
                    //     console.log(block.transactions.forEach(function(item){
                    //         console.log(item.toObject());
                    //     }));
                    // });
                });

                // node.services.bitcoind.on('block', function (blockHash) {
                //     console.log(new Date(), blockHash);
                //     var blockHashHax = bufferUtil.bufferToHex(blockHash);
                //     node.services.bitcoind.getBlockHeader(blockHashHax, function(err, blockHeader) {
                //         console.log("blockHashHax:", blockHeader);
                //     });
                //     updateBlocksNow(node, reOneCol, reTwoCol, blocksCol, blockHashHax, function (err, res) {
                //         if(err) return console.log("updateBlocksNow Err:", err);
                //         console.log("updateBlocksNow:", res);
                //     });
                //     updateBlockStatistic(node, blockDetailsCol, blockHashHax, function (err, res) {
                //         if(err) return console.log("updateBlockStatistic:", err);
                //         console.log(res);
                //     });
                // })
            }
            catch (e) {
                console.log("getBestBlockHash or getBlockOverview Err:", e)
            }

        })
});

//double spend
var updateBlockList = function (blocks, colName, callback) {
    console.log("tx.length", blocks.transactions.length);
    async.each(blocks.transactions, function (tx, done) {
        //console.log(tx.toObject().inputs)
        async.each(tx.toObject().inputs, function (input, next) {
            colName.insertOne({
                prevTxId: input.prevTxId,
                outputIndex: input.outputIndex,
                hash: tx.hash,
                blockHash: blocks.hash,
                date: new Date()
            });
            next();
        }, function (err) {
            if (err) callback(err);
            done();
        })
    }, function (err) {
        callback(err, "Done");
    })
}
//connection to Db`s
var connectToDb = function (connectionPath, cb) {
    MongoClient.connect(connectionPath, {
        db: {bufferMaxEntries: 0},
        server: {
            reconnectTries: 30000,
            reconnectInterval: 1000,
            auto_reconnect: true
        }
    }, function (err, DB) {
        cb(null, DB);
    })
};
var updateBlocksNow = function () {
    if (arguments) {
        var node = arguments[0],
            reOneCol = arguments[1],
            reTwoCol = arguments[2],
            blocksCol = arguments[3],
            blockNumber = arguments[4],
            cb = arguments[5];
        node.services.bitcoind.getBlockOverview(blockNumber, function (err, blockOverview) {
            if (err) throw err;
            try {
                async.parallel({
                    re1: function (cb) {
                        reOneCol.update(
                            {
                                "hash": {$in: blockOverview.txids}
                            },
                            {
                                $set: {
                                    "blockHash": blockOverview.hash,
                                    "previousBlock": blockOverview.prevHash,
                                    "blockTime": blockOverview.time,
                                    "blockNumber": blockOverview.height
                                }
                            },
                            {
                                multi: true
                            }, function (err) {
                                if (err)
                                    cb(null);
                                else
                                    cb(null, blockOverview.hash);
                            }
                        );
                    },
                    re2: function (cb) {
                        reTwoCol.update(
                            {
                                "hash": {$in: blockOverview.txids}
                            },
                            {
                                $set: {
                                    "blockHash": blockOverview.hash,
                                    "previousBlock": blockOverview.prevHash,
                                    "blockTime": blockOverview.time,
                                    "blockNumber": blockOverview.height
                                }
                            },
                            {
                                upsert: true,
                                multi: true
                            }, function (err) {
                                if (err)
                                    cb(null);
                                else
                                    cb(null, blockOverview.hash);
                            }
                        )
                    },
                    block: function (cb) {
                        blocksCol.findOneAndUpdate(
                            {
                                "blockHash": blockOverview.hash
                            },
                            {
                                $set: {
                                    "blockHash": blockOverview.hash,
                                    "previousBlock": blockOverview.prevHash,
                                    "blockTime": blockOverview.time,
                                    "blockNumber": blockOverview.height
                                }
                            },
                            {
                                upsert: true
                            }, function (err) {
                                if (err)
                                    cb(null);
                                else
                                    cb(null, blockOverview.hash)
                            }
                        );

                    }
                }, function (err, results) {
                    console.log("re1:", results.re1);
                    console.log("re2:", results.re2);
                    console.log("block:", results.block);
                    cb(err, results);
                })

            }
            catch (e) {
                console.log("updatesBlockHash Err:", e);
            }
        });

    }
}
var updateBlockStatistic = function () {
    //console.log(arguments);
    if (arguments) {
        var node = arguments[0],
            blockDetailsCol = arguments[1],
            blockNumber = arguments[2],
            cb = arguments[3];
        node.services.bitcoind.getBlockOverview(blockNumber, function (err, blockOverview) {
            if (err) throw err;
            var data = {hash: blockOverview.hash};
            data.height = blockOverview.height;
            data.blockTime = blockOverview.time;
            data.previousBlock = blockOverview.prevHash;
            console.log("data:", data);
            data.transactions = blockOverview.txids;
            node.services.bitcoind.getRawBlock(blockNumber, function (err, blockBuffer) {
                if (err) throw err;
                data.size = blockBuffer.length;
                updateBlockDetails(node, blockDetailsCol, data, function (err, updatedBlockNumber) {
                    console.log("UpdatedBlockNumber1:", updatedBlockNumber);
                    cb(err, updatedBlockNumber);
                    // node.services.bitcoind.getBlockOverview(data.previousBlock, function (err, blockOverview) {
                    //         if (err) throw  err;
                    //         try {
                    //             var dataPrev = {hash: blockOverview.hash};
                    //             dataPrev.height = blockOverview.height;
                    //             dataPrev.blockTime = blockOverview.time;
                    //             dataPrev.previousBlock = blockOverview.prevHash;
                    //             console.log("dataPrev:",dataPrev);
                    //             dataPrev.transactions = blockOverview.txids;
                    //             // node.services.bitcoind.getRawBlock(dataPrev.hash, function (err, blockBuffer) {
                    //             //     if (err) throw err;
                    //             //     dataPrev.size = blockBuffer.length;
                    //             //     // updateBlockDetails(node, colName3, dataPrev, function (err, res) {
                    //             //     //     if(err) throw err;
                    //             //     //     console.log("res2", res);
                    //             //     // });
                    //             // })
                    //
                    //             reOneCol.update(
                    //                 {
                    //                     "hash": {$in: blockOverview.txids}
                    //                 },
                    //                 {
                    //                     $set: {
                    //                         "blockHash": blockOverview.hash,
                    //                         "previousBlock": blockOverview.prevHash,
                    //                         "blockTime": blockOverview.time,
                    //                         "blockNumber": blockOverview.height
                    //                     }
                    //                 },
                    //                 {
                    //                     multi: true
                    //                 }
                    //             );
                    //
                    //             reTwoCol.update(
                    //                 {
                    //                     "hash": {$in: blockOverview.txids}
                    //                 },
                    //                 {
                    //                     $set: {
                    //                         "blockHash": blockOverview.hash,
                    //                         "previousBlock": blockOverview.prevHash,
                    //                         "blockTime": blockOverview.time,
                    //                         "blockNumber": blockOverview.height
                    //                     }
                    //                 },
                    //                 {
                    //                     multi: true
                    //                 }
                    //             )
                    //         }
                    //         catch (e) {
                    //             console.log("last Block Update Err:", e)
                    //         }
                    //
                    //     });
                });
            });

        })

    }
    else {
        console.log("arguments:", arguments);
    }

}
var updateBlockDetails = function (node, collectionName, data, callback) {
    console.log("txIds.Length:", data.transactions.length)
    var fee, size, numbers = [],
        transactions = data.transactions,
        blockDetails;
    collectionName.find(
        {blockHash: data.previousBlock},
        {blockTime: true}
    ).toArray(function (err, prevBlock) {
        try {
            var timeGap;
            if (!err && prevBlock.length)
                timeGap = Number(( (data.blockTime - prevBlock[0].blockTime / 1000) / 60).toFixed(2));
            else
                timeGap = 0;
            async.eachSeries(transactions, function (txId, cb) {
                    try {
                        node.services.bitcoind.getDetailedTransaction(txId, function (err, tx) {
                            if (err) throw err;
                            if (tx) {
                                fee = tx.feeSatoshis;
                                size = tx.hex.length / 2;
                                if (fee > 0 && size > 0) {
                                    numbers.push(fee / size);
                                }
                                cb();
                            }
                            else {
                                cb();
                            }
                        })
                    }
                    catch (e) {
                        console.log("fee Err:", e)
                    }

                },
                function (err) {
                    if (err) throw err;
                    console.log("numbers", numbers.length)
                    if (numbers.length > 10)
                        blockDetails = {
                            blockHash: data.hash,
                            blockNumber: data.height,
                            blockTime: data.blockTime * 1000,
                            blockSize: data.size,
                            blockTimeGap: timeGap,
                            median: stats.median(numbers),
                            stdev: stats.stdev(numbers),
                            average: stats.mean(numbers),
                            min: Math.min.apply(null, numbers),
                            quartile1: getQuartile1(numbers),
                            quartile3: getQuartile3(numbers)
                        };
                    else
                        blockDetails = {
                            blockHash: data.hash,
                            blockNumber: data.height,
                            blockTime: data.blockTime * 1000,
                            blockSize: data.size,
                            blockTimeGap: timeGap,
                            fewTrxBlock: true
                        };

                    if (collectionName && blockDetails) {

                        collectionName.update({blockHash: blockDetails.blockHash},
                            {$set: blockDetails}, {upsert: true},
                            function (err, data) {
                                if (err) callback(err);
                                var i = 30;

                                if (data)
                                    collectionName.find({
                                        lastBlocksAverage: {$exists: false},
                                        fewTrxBlock: {$exists: false}
                                    }).sort({_id: -1}).limit(i).toArray(function (err, data) {
                                        if (data.length) {
                                            async.parallel([
                                                function (cb) {
                                                    collectionName.update(
                                                        {lastBlocksAverage: i},
                                                        {$set: getBlockAverages(data, i, blockDetails)},
                                                        {upsert: true},
                                                        function (err, res) {
                                                            if (err) cb(err);
                                                            cb(null, res)
                                                        }
                                                    );
                                                },
                                                function (cb) {
                                                    collectionName.update(
                                                        {lastBlocksAverage: 6},
                                                        {$set: getBlockAverages(data, 6, blockDetails)},
                                                        {upsert: true},
                                                        function (err, res) {
                                                            if (err) cb(err);
                                                            cb(null, res)
                                                        }
                                                    )
                                                }
                                            ], function (err) {
                                                if (err) callback(err);
                                                console.log("end save", new Date());
                                                callback(null, blockDetails.blockNumber);
                                            })
                                        }
                                    })
                            });
                    }
                })
        }
        catch (e) {
            console.log("Err:", e)
        }

    })
}
function getQuartile1(numbers) {
    var nums = numbers.sort(function (a, b) {
        return a - b
    });

    return stats.median(nums.slice(0, Math.floor(nums.length / 2)));
}
function getQuartile3(numbers) {
    var nums = numbers.sort(function (a, b) {
        return a - b
    });

    return stats.median(nums.slice(Math.ceil(nums.length / 2)));
}
function getBlockAverages(data, i, block) {
    var arr = data.slice(0, i);

    return {
        blockNumber: block.blockNumber,
        blockTime: block.blockTime,
        blockSize: arr.reduce(function (pv, cv) {
            return (pv || 0) + (cv.blockSize || 0);
        }, 0) / i,
        blockTimeGap: block.blockTimeGap,
        median: arr.reduce(function (pv, cv) {
            return (pv || 0) + (cv.median || 0);
        }, 0) / i,
        stdev: arr.reduce(function (pv, cv) {
            return (pv || 0) + (cv.stdev || 0);
        }, 0) / i,
        average: arr.reduce(function (pv, cv) {
            return (pv || 0) + (cv.average || 0);
        }, 0) / i,
        min: arr.reduce(function (pv, cv) {
            return (pv || 0) + (cv.min || 0);
        }, 0) / i,
        quartile1: arr.reduce(function (pv, cv) {
            return (pv || 0) + (cv.quartile1 || 0);
        }, 0) / i,
        quartile3: arr.reduce(function (pv, cv) {
            return (pv || 0) + (cv.quartile3 || 0);
        }, 0) / i
    };
}

