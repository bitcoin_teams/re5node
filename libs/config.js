module.exports = {
    replicaSetDB: "mongodb://176.58.96.173:27017," +
               "45.33.95.186:27017,"  +
               "109.237.26.114:27017/riskEngineDB?replicaSet=reReplica",
    monitoringDB: 'mongodb://139.162.254.251:27017/monitoringDB',
    s1DB: 'mongodb://185.69.55.167:27017/riskEngineDB',
    testDB: 'mongodb://185.69.55.167:27017/testDB',
    memPoolDB: 'mongodb://139.162.254.251:27017/riskEngineFiveDB',
    riskEngineDB: 'mongodb://127.0.0.1:27017/riskEngineDB',
    riskEngineReplicaDB: "mongodb://139.162.254.46:27017," +
                         "45.33.95.186:27017,"  +
                         "109.237.26.114:27017/riskEngineDB?replicaSet=reReplica",
    riskEngineTestDB: 'mongodb://reiskEngineUsr:Z8PzWx796aDWsAhp@127.0.0.1:27017/riskEngineDB',
    monitoringTestDB: 'mongodb://monitoringUsr:NmAq36d8jvsRRtjN@127.0.0.1:27017/monitoringDB',
    AccountManagementTestDb: 'mongodb://AccountManagementUsr:gqmxkDf3GwwnyusH@127.0.0.1:27017/AccountManagementDb',
    localDb: 'mongodb://127.0.0.1:27017/testDb',
    accountMen2Db: 'mongodb://139.162.230.146:27017/testDb',
    re5Db: 'mongodb://139.162.230.146:27017/testDb',
    riskEngineFiveDB: 'mongodb://109.237.26.114:27017/riskEngineDB',
    riskEngineOneDB: 'mongodb://176.58.96.173:27017/riskEngineDB',
    riskEngineTwoDB: 'mongodb://139.162.220.128:27017/riskEngineDB',
    mainCol: 'transactions_re5',
    p2pCol: 'p2pDb',
    memPoolCol: 'mempool',
    insightApiPath: 'http://139.162.254.251/',
    blockDetails: 'BlockDetails',
    doubleSpends: 'DoubleSpends',
    blockChainInfoPath: 'https://blockchain.info/rawblock/',
    blockTrailPath: 'https://api.blocktrail.com/v1/btc/block/',
    blockTrailKey: '/transactions?api_key=49b0d6f360c3575c1baa2e6103fcee04df32208b',
    redisConf: {host: '139.162.254.251', port: 6379}
};