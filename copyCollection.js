'use strict';
'use strict';
var cron = require('node-cron');
var async = require('async');
var Promise = require("bluebird");
var config = require('./libs/config');
var MongoClient = require('mongodb').MongoClient
    , assert = require('assert');
var monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"
];
//connection to Db`s
var connectToDb = function (connectionPath, cb) {
    var options = {
        server: {
            socketOptions: {
                keepAlive: 1,
                connectTimeoutMS: 30000,
                socketTimeoutMS: 90000
            }
        },
        replset: {
            socketOptions: {
                keepAlive: 1,
                connectTimeoutMS: 30000,
                socketTimeoutMS: 90000
            }
        }
    };
    MongoClient.connect(connectionPath, options, function (err, db) {
        cb(err, db);
    })
};
//copy Collection
var copyCollection = function () {
    if (arguments) {
        var mainCol = arguments[0],
            weeklyCol = arguments[1],
            Query = arguments[2];
        return new Promise(function (resolve, reject) {
            mainCol.find(Query).forEach(function (item) {
                weeklyCol.insertOne(item, function (err, doc) {
                    if (!err) {
                        mainCol.remove({_id: doc.insertedId}, function (err) {
                            if (err) reject(err);
                            resolve();
                        });
                    }
                });
            })
        })
    }
};

var copyMonthCollection = function () {
    var mainCol = arguments[0];
    var s1MonthCol = arguments[1];
    var Query = arguments[2];
    return new Promise(function (resolve, reject) {
        mainCol.find(Query).forEach(function (item) {
            s1MonthCol.insertOne(item, function (err) {
                if (!err) {
                    resolve();
                }
                else if(err.code != 11000){
                    reject(err);
                }


            });
        })
    })
};
var getCollectionName = function () {
    var year = new Date().getFullYear(),
        month = new Date().getMonth() + 1,
        day = new Date().getDate()

    return 'trxs_' + day + '.' + month + '.' + year;
};
var getMonthColName = function () {
    var date = new Date();
    var monthName = monthNames[date.getMonth()];
    return monthName + date.getFullYear();
};
var s1_month_col_name = getMonthColName();
var week_collection = getCollectionName();
var last_week_collection = week_collection;
process.on('uncaughtException', function (err) {
    console.error('Caught exception: ' + err);
});


async.parallel({
    RE: connectToDb.bind(null, config.riskEngineDB),
    S1: connectToDb.bind(null, config.testDB)
}, function (err, DBs) {
    assert.equal(null, err);
    console.log("Connected to riskEngine DB!");
    console.log("week_collection:", week_collection);
    console.log("monthColName", s1_month_col_name);

    var mainCol = DBs.RE.collection(config.mainCol),
        weekCol = DBs.RE.collection(week_collection),
        lastWeekCol = DBs.RE.collection(last_week_collection);
    weekCol.createIndex({"hash": 1}, {dropDups: true, unique: true});
    lastWeekCol.createIndex({"hash": 1}, {dropDups: true, unique: true});

    var s1WeekCol = DBs.S1.collection(week_collection),
        s1LastWeekCol = DBs.S1.collection(last_week_collection);
    s1WeekCol.createIndex({"hash": 1}, {dropDups: true, unique: true});
    s1LastWeekCol.createIndex({"hash": 1}, {dropDups: true, unique: true});

    var s1MonthCol = DBs.S1.collection(s1_month_col_name);
    s1MonthCol.createIndex({"hash": 1}, {dropDups: true, unique: true});
    //running every first day of the month 00:01 / '1 0 1 * *'
    cron.schedule('1 0 1 * *', function () {
        s1_month_col_name = getMonthColName();
        s1MonthCol = DBs.S1.collection(s1_month_col_name);
        s1MonthCol.createIndex({"hash": 1}, {dropDups: true, unique: true});
    })
    //running every Monday 00:00 / '0 0 * * 1'
    cron.schedule('0 0 * * 1', function () {
        try {
            lastWeekCol = weekCol;
            week_collection = getCollectionName();

            // weekCol = DBs.RE.collection(week_collection);
            // weekCol.createIndex({"hash": 1}, {dropDups: true, unique: true});

            s1WeekCol = DBs.S1.collection(week_collection);
            s1WeekCol.createIndex({"hash": 1}, {dropDups: true, unique: true});

            console.log("Weekly Collections created..");
        }
        catch (e) {
            console.log("running every Monday 00:00 Err:", e);
        }

    });

    //running every Thursday 00:01 / '1 0 * * 4'
    cron.schedule('1 0 * * 4', function () {
        try {
            //lastWeekCol = weekCol;
            s1LastWeekCol = s1WeekCol;
            console.log("changed last Weekly Collections..");
        }
        catch (e) {
            console.log("running every Thursday 00:01 Err:", e);
        }

    });

    //running every day 00:02 / '2 0 * * *'
    cron.schedule('2 0 * * *', function () {
        var D = new Date();
        D.setDate(D.getDate() - 3);
        var Query = {
            $and: [
                {date: {$lt: D}},
                {
                    $or: [
                        {blockHash: {$exists: false}},
                        {txStaticInfo: {$exists: false}}
                    ]
                }
            ]
        };

        copyCollection(mainCol, s1LastWeekCol, Query).then(function () {
            console.log("End running every day 00:02 on S1..");
        });


    });

    //running every 30 minutes */30 * * * *
    cron.schedule('*/30 * * * *', function () {
        var D = new Date();
        D.setMinutes(D.getMinutes() - 30);
        var Query = {
            $and: [
                {date: {$lt: D}},
                {blockHash: {$exists: true}}
            ]
        };
        var query = {
            $and: [
                {date: {$lt: D}},
                {userAccount: {$exists: true}}
            ]
        };

        copyMonthCollection(mainCol, s1MonthCol, query).then(function () {
            console.log("userAccount End running every 30 minutes on S1..");
        })

        copyCollection(mainCol, s1WeekCol, Query).then(function () {
            console.log("End running every 30 minutes on S1..");
        });


    });

})

// MongoClient.connect(config.riskEngineDB, {
//     db: {bufferMaxEntries: 0},
//     server: {
//         reconnectTries: 30000,
//         reconnectInterval: 1000,
//         auto_reconnect: true
//     }
// }, function (err, db) {
//     assert.equal(null, err);
//     console.log("Connected to riskEngine DB!");
//     console.log("week_collection:", week_collection);
//     var mainCol = db.collection(config.mainCol),
//         weekCol = db.collection(week_collection),
//         lastWeekCol = db.collection(last_week_collection);
//     weekCol.createIndex({"hash": 1}, {dropDups: true, unique: true});
//     lastWeekCol.createIndex({"hash": 1}, {dropDups: true, unique: true});
//     //running every Monday 00:00 / '0 0 * * 1'
//     cron.schedule('0 0 * * 1', function () {
//         try {
//             //last_week_collection = week_collection;
//             lastWeekCol = weekCol;
//             week_collection = getCollectionName();
//             weekCol = db.collection(week_collection);
//             weekCol.createIndex({"hash": 1}, {dropDups: true, unique: true});
//
//             console.log("Weekly Collection created..");
//         }
//         catch (e) {
//             console.log("running every Monday 00:00 Err:", e);
//         }
//
//     });
//
//     //running every Thursday 00:01 / '1 0 * * 4'
//     cron.schedule('1 0 * * 4', function () {
//         try {
//             lastWeekCol = weekCol;
//             console.log("changed last Weekly Collection..");
//         }
//         catch (e) {
//             console.log("running every Thursday 00:01 Err:", e)
//         }
//
//     });
//
//     //running every day 00:02 / '2 0 * * *'
//     cron.schedule('2 0 * * *', function () {
//         var D = new Date();
//         D.setDate(D.getDate() - 3);
//         var Query = {
//             $and: [
//                 {date: {$lt: D}},
//                 {
//                     $or: [
//                         {blockHash: {$exists: false}},
//                         {txStaticInfo: {$exists: false}}
//                     ]
//                 }
//             ]
//         };
//         copyCollection(mainCol,lastWeekCol,Query).then(function () {
//             console.log("End running every day 00:02..");
//         })
//     });
//
//     //running every 30 minutes
//     cron.schedule('*/30 * * * *', function () {
//         var D = new Date();
//         D.setMinutes(D.getMinutes() - 30);
//         var Query = {
//             $and: [
//                 {date: {$lt: D}},
//                 {blockHash: {$exists: true}}
//             ]
//         };
//         copyCollection(mainCol,weekCol, Query).then(function () {
//             console.log("End running every 30 minutes..")
//         })
//     });
//
// });

