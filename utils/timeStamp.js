'use strict';
module.exports = {
    getTimeStamp: function getTimeStamp(){
        var ts = String(Math.round(new Date().getTime() / 1000));
        return ts;
    },

    getTimeInventory: function getTimeInventory(){
        var time= new Date();
        var strDateTime = [[AddZero(time.getDate()), AddZero(time.getMonth() + 1),
            time.getFullYear()].join("/"), [AddZero(time.getHours()),
            AddZero(time.getMinutes()),AddZero(time.getSeconds()),AddZero(time.getMilliseconds())].join(":"),
            time.getHours() >= 12 ? "PM" : "AM"].join(" ");

        function AddZero(num) {
            return (num >= 0 && num < 10) ? "0" + num : num + "";
        }

        return strDateTime;
    }

}