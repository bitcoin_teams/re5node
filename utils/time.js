'use strict';
var time = require('time');

exports.getGMT = function () {
    var b = new time.Date();
    return b.toString();
}
