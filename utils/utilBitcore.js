'use strict';
var bitcoin = require('bitcore-lib');

module.exports ={
    OpReturn: function(outputs){
        var respons = [];
        outputs.forEach(function(item){
            var script = bitcoin.Script(item.script);
            respons.push(script.isDataOut());
        })

        return respons;
    },

    MultisigOut: function(outputs){
        var respons = [];
        outputs.forEach(function(item){
            var script = bitcoin.Script(item.script);
            //console.log("MultisigOut", script.isMultisigOut())
            respons.push(script.isMultisigOut());
        })

        return respons;
    },

    MultisigIn: function(inputs){
        var respons = [];
        inputs.forEach(function(item){
            var script = bitcoin.Script(item._scriptBuffer);
           // console.log("MultisigIn", script.isMultisigIn())
            respons.push(script.isMultisigIn());
        })

        return respons;
    },

    getTransactionValue: function(Inputs, Outputs, valueOut){
        for(var i =0;i < Outputs.length; ++i){
            for(var j =0; j < Inputs.length; ++j){
                if(Outputs[i].address == Inputs[j].address){
                    valueOut -= Outputs[i].satoshis;
                    break;
                }
            }
        }

        return valueOut/100000000;
    },

    getForMemPoolList: function(Inputs){
        var mempoolList = [];
        for(var i = 0; i < Inputs.length; ++i){
            mempoolList.push({txId:Inputs[i].prevTxId, outputIndex: Inputs[i].outputIndex})
        }

        return mempoolList;
    }
}