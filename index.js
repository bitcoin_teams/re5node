"use strict"
var bitcore = require('bitcore-node');
var stats = require("stats-lite");
var lib = require('bitcore-lib');
var MongoClient = require('mongodb').MongoClient;
var redis = require("redis");
var request = require('request');
var Queue = require('queuejs');
var assert = require('assert');
var async = require('async');
var config = require('./libs/config');
var util = require('./utils/utilBitcore');
var time = require('./utils/time');
var timeStamp = require('./utils/timeStamp');
var bufferUtil = lib.util.buffer;
var memPool = redis.createClient();
var blockDetails = redis.createClient(config.redisConf);
var rstats = require('rstats');
var R = new rstats.session();
//R.parseEvalQ("source('/home/ruser/Gap600/GAP600.R')");
R.parseEvalQ("source('/home/ruser/GAP600_20170222.R')");

//Services
var Bitcoin = bitcore.services.Bitcoin;

var configuration = {
    network: 'livenet',
    services: [
        {
            name: 'bitcoind',
            module: Bitcoin,
            config: {
                spawn: {
                    "datadir": "/root/.bitcore/data",
                    "exec": "/root/.nvm/versions/node/v0.12.10/lib/node_modules/bitcore/node_modules/bitcore-node/bin/bitcoind"
                }
            }
        }
    ]
}
var node = new bitcore.Node(configuration);
// process.on('uncaughtException', function (err) {
//     console.error('Caught exception: ' + err);
// });

node.start(function (err) {
    if (err) {
        assert.equal(null, err)
        node.stop(function () {
            console.log('shutdown the node');
        });
    }
});
node.on('error', function (err) {
    assert.equal(null, err);
    node.stop();
});

node.on('ready', function () {
    async.parallel({
            RE: connectToDb.bind(null, config.riskEngineReplicaDB),
            RE5: connectToDb.bind(null, config.riskEngineDB),
            MDB: connectToDb.bind(null, config.monitoringDB)
        },
        function (err, DBs) {
            assert.equal(null, err);
            console.log("Connected correctly to Dbs");
            var mainCol = DBs.RE.collection('transactions_replica');
            var testColl = DBs.RE5.collection("transactions_re5");
            mainCol.createIndex({"hash": 1}, {dropDups: true, unique: true});
            node.services.bitcoind.on('tx', function (transactionBuffer) {
                var trx = lib.Transaction().fromBuffer(transactionBuffer);
                node.services.bitcoind.getDetailedTransaction(trx.hash, function (err, transaction) {
                    if (transaction != undefined) {
                        if (transaction.coinbase == true) {
                            updateCoinBase(mainCol, transaction);
                        }
                        else {
                            async.parallel([
                                    insertMemPool.bind(null, transaction),
                                    staticInfo.bind(null, transaction),
                                    blockStatistic.bind(null, transaction.hash),
                                    confirmationList.bind(null, node, transaction)
                                ],
                                function (err, results) {
                                    assert.equal(err, null);
                                    var dataForUpdate = {
                                        hash: results[0],
                                        date: new Date(),
                                        txValue: results[1].txValue,
                                        txStaticInfo: results[1].staticInfo,
                                        lastBlocksAverage30: results[2].lastBlocksAverage30,
                                        lastBlocksAverage6: results[2].lastBlocksAverage6,
                                        lastBlocksAverage: results[2].lastBlocksAverage,
                                        previousBlock: results[2].previousLastBlock,
                                        previousSecondBlock: results[2].previousSecondBlock,
                                        confirmationList: results[3],
                                    };



                                    //console.log("confirmationList:", dataForUpdate.confirmationList);
                                    levelOrderTraversal(dataForUpdate.confirmationList, mainCol, dataForUpdate.hash, function (err, RBF) {
                                        if(err) console.error(err);
                                        console.log(RBF);
                                        if(RBF.rbf == 'rbfAncestor'){
                                                console.log("RBF.rbf == 'rbfAncestor' ")
                                            dataForUpdate.rbfAncestor = true;
                                            dataForUpdate.level = level;

                                        }
                                        if(RBF.rbf == 'rbfAncestorSuspected'){
                                            console.log(RBF.rbf == 'rbfAncestorSuspected')
                                            dataForUpdate.rbfAncestor == "Suspected";
                                            dataForUpdate.level == RBF.level
                                        }



                                        updateAllData(testColl, dataForUpdate, function (err, hash) {
                                            assert.equal(err, null);
                                            try {

                                                R.assign('hash', hash);
                                                // var i = 0, riskScores = {}, s,
                                                // scores = R.parseEval("GetGap600Score20170222(hash)")[0].split(",");
                                                // for(i in scores){
                                                //     s = scores[i].split(".");
                                                //     riskScores[s[0]] = s[1];
                                                // }

                                                // console.log(hash);
                                                // console.log(riskScores);

                                                mainCol.updateOne({hash: hash}, {
                                                    $set: {
                                                        'riskScore': R.parseEval("GetGap600Score20161027(hash)")[0],
                                                        'scoreTime': time.getGMT()
                                                    }
                                                })
                                            }

                                            catch (e) {
                                                console.log("riskScore Err:", e)
                                                mainCol.updateOne({"hash": hash}, {
                                                    $set: {
                                                        'riskScore': {},
                                                        'scoreTime': time.getGMT()
                                                    }
                                                });
                                            }

                                        })
                                    });

                                    // dataForUpdate.confirmationList.forEach(function (item) {
                                    //     if(item.confirmation == 0 && item.sequence == 4294967295){
                                    //         testColl.insertOne({hash: item.hash, rbfAncestorSuspected: true });
                                    //         node.services.bitcoind.getTransaction(item.hash, function(err, transaction) {
                                    //             if(transaction){
                                    //                 transaction.inputs.forEach(function(input){
                                    //                     var prevTxId = input.toObject().prevTxId;
                                    //                     // getConfirmation(node, prevTxId, function (err, confirmation) {
                                    //                     //     if(err) throw err;
                                    //                     //     console.log({hash:dataForUpdate.hash, confirmation: confirmation, sequenceNumber: input.sequenceNumber });
                                    //                     //     if(confirmation==0 && input.sequenceNumber < 4294967295 ){
                                    //                     //         dataForUpdate.rbfAncestor = true
                                    //                     //     }
                                    //                     //     if(confirmation == 0 && input.sequenceNumber == 4294967295){
                                    //                     //
                                    //                     //     }
                                    //                     // });
                                    //
                                    //                     node.services.bitcoind.getDetailedTransaction(prevTxId, function (err, trx) {
                                    //                         var txHeight = 0;
                                    //                         if (trx) {
                                    //                             if (trx.height + 1 > 0) {
                                    //                                 txHeight = (node.services.bitcoind.height - trx.height) + 1;
                                    //                             }
                                    //                             //console.log("txHeight",txHeight)
                                    //                             //console.log(transaction.toObject());
                                    //                             if(txHeight == 0 && item.sequence == 4294967295){
                                    //                                 testColl.insertOne({hash: item.hash, rbfAncestorSuspected: true });
                                    //                             }
                                    //
                                    //                             if(txHeight == 0 && input.sequenceNumber < 4294967295){
                                    //                                 dataForUpdate.rbfAncestor = true;
                                    //                                 console.log("rbf:", dataForUpdate.rbfAncestor);
                                    //                                 testColl.insertOne({hash:dataForUpdate.hash, rbfAncestor: true })
                                    //                             }
                                    //
                                    //                             console.log("Input.Input.Confirmation:", {hash: trx.hash, confirmation: txHeight, sequence: input.sequenceNumber})
                                    //
                                    //                         }
                                    //                     })
                                    //
                                    //                     // node.services.bitcoind.getDetailedTransaction(input.toObject().prevTxId, function (err, trx) {
                                    //                     //     var txHeight = 0;
                                    //                     //     if (trx) {
                                    //                     //         if (trx.height + 1 > 0) {
                                    //                     //             txHeight = (node.services.bitcoind.height - trx.height) + 1;
                                    //                     //         }
                                    //                     //
                                    //                     //         //console.log(transaction.toObject());
                                    //                     //         if(txHeight == 0 && input.sequenceNumber < 4294967295){
                                    //                     //             dataForUpdate.rbfAncestor = true;
                                    //                     //             console.log("rbf:", dataForUpdate.rbfAncestor);
                                    //                     //             testColl.insertOne({hash:dataForUpdate.hash, rbf: true })
                                    //                     //         }
                                    //                     //
                                    //                     //         console.log("Input.Input.Confirmation:", {hash: trx.hash, confirmation: txHeight, sequence: input.sequenceNumber})
                                    //                     //
                                    //                     //     }
                                    //                     // })
                                    //                 })
                                    //             }
                                    //         });
                                    //     }
                                    // })



                                });
                        }

                    }

                });
            });

            // node.services.bitcoind.on('block', function (blockHash) {
            //     console.log(new Date(), blockHash)
            //     var blockHashHax = bufferUtil.bufferToHex(blockHash);
            //     setTimeout(function () {
            //         node.services.bitcoind.getBlockOverview(blockHashHax, function (err, blockOverview) {
            //             if (err) {
            //                 return console.log(err);
            //             }
            //             try {
            //                 mainCol.update(
            //                     {
            //                         "hash": {$in: blockOverview.txids}
            //                     },
            //                     {
            //                         $set: {
            //                             "blockHash": blockOverview.hash,
            //                             "previousBlock": blockOverview.prevHash,
            //                             "blockTime": blockOverview.time,
            //                             "blockNumber": blockOverview.height
            //                         }
            //                     },
            //                     {
            //                         multi: true
            //                     }, function (err) {
            //                         if (err) return console.log(err);
            //                     }
            //                 );
            //             }
            //             catch (e) {
            //                 console.log("update BlockHash Err:", e);
            //             }
            //         });
            //
            //     }, 2000)
            // });
        })
});

//Level-order traversal
var levelOrderTraversal = function (treeList, colName, hash, next) {
    //console.time('startTime');
    console.log("treeList: ", treeList.length);
    var queue = new Queue(),
        level = 0;
    treeList.forEach(function (root) {
        if(root.confirmation == 0 && root.sequenceNumber < 4294967295 ){
            console.log({hash: hash, level:level, rbf:'rbfAncestor'})
            return next(null, {hash: hash, level:level, rbf:'rbfAncestor'});
        }

        if(root.confirmation == 0 && root.sequenceNumber == 4294967295){
            console.log(root);
            queue.enq(root.hash);
        }

    })

    console.log(queue._elements);
    while (!queue.isEmpty()){
        var firstHash = queue.deq();
        console.log("childrenHash:", firstHash);
        if(level == 2) return next(null, {hash: hash, rbf: "rbfAncestorSuspected"});
        colName.findOne({ hash:firstHash }, { confirmationList: true }, function (err, doc) {
            if(err) next(err);
            if(doc != null){
                level++;
                console.log("children", doc);
                var children = doc.confirmationList;
                for(var i = 0; i <= children.length; ++i){
                      if(children[i].confirmation == 0 && children[i].sequenceNumber < 4294967295 ) return next(null, {hash: hash, level:level, rbf:'rbfAncestor'});

                      if(children[i].confirmation == 0 && children[i].sequenceNumber == 4294967295) queue.enq(children.hash);
                }
            }
        })
    }

    next(null, {hash: hash, level:level, rbf: false});

   // console.timeEnd("startTime");
}

var getConfirmation = function (node, prevTxId, cb) {
    node.services.bitcoind.getDetailedTransaction(prevTxId, function (err, trx) {
        var txHeight = 0;
        if (trx) {
            if (trx.height + 1 > 0) {
                txHeight = (node.services.bitcoind.height - trx.height) + 1;
            }
            cb(err, txHeight);
            //console.log(transaction.toObject());
            // if(txHeight == 0 && input.sequenceNumber < 4294967295){
            //     dataForUpdate.rbfAncestor = true;
            //     console.log("rbf:", dataForUpdate.rbfAncestor);
            //     testColl.insertOne({hash:dataForUpdate.hash, rbf: true })
            // }
        }
    })
}
//connection to Db`s
var connectToDb = function (connectionPath, cb) {
    MongoClient.connect(connectionPath, {
        db: {bufferMaxEntries: 0},
        server: {
            reconnectTries: 30000,
            reconnectInterval: 1000,
            auto_reconnect: true
        }
    }, function (err, r5DB) {
        cb(err, r5DB);
    })
};
//updates in main collection coin base transactions..
var updateCoinBase = function (collectionName, transaction) {
    try {
        collectionName.update(
            {hash: transaction.hash},
            {
                hash: transaction.hash,
                date: new Date(),
                txValue: transaction.outputSatoshis,
                coinbase: transaction.coinbase,
                txStaticInfo: {
                    inputs: transaction.inputs
                    , outputs: transaction.outputs
                    , valueOut: transaction.outputSatoshis
                    , size: transaction.hex.length / 2
                    , time: timeStamp.getTimeInventory()
                    , location: "LND"
                },
                blockHash: transaction.blockHash,
                blockNumber: transaction.height,
                blockTime: transaction.blockTimestamp

            },
            {
                upsert: true
            });
    }
    catch (e) {
        console.error("coinBase Err:", e);
    }
};
//process One, insert transaction Hash and new Date in main collection..
var insertMemPool = function (transaction, callback) {
    var outputJson = {
        hash: transaction.hash,
        outputs: transaction.outputs
    }
    var inputs = transaction.inputs;
    try {
        memPool.set([transaction.hash, transaction.hash, 'EX', 30, 'NX'], function (err) {
            if (err) throw err;
            async.eachSeries(inputs, function (input, cb) {
                memPool.set([input.prevTxId + "#" + input.outputIndex, JSON.stringify(outputJson), 'EX', 30, 'NX'], function (err) {
                    if (err) cb(err);
                    cb()
                });
            }, function (err) {
                if (err) throw err;
                callback(null, transaction.hash);
            })
        });

    }
    catch (e) {
        console.log("Redis Err:", e);
        callback(e);
    }
}
// process Two updates static info for same tx hash..
var staticInfo = function (transaction, callback) {
    try {

        callback(null, {
            staticInfo: {
                inputs: transaction.inputs,
                valueIn: transaction.inputSatoshis,
                outputs: transaction.outputs,
                valueOut: transaction.outputSatoshis,
                fees: transaction.feeSatoshis,
                size: transaction.hex.length / 2,
                fee_size_ratio: transaction.feeSatoshis / (transaction.hex.length / 2),
                fee_totalInputValue_ratio: transaction.feeSatoshis / transaction.inputSatoshis,
                time: timeStamp.getTimeInventory(),
                location: "LND",
                OP_RETURN: util.OpReturn(transaction.outputs),
                multisigOut: util.MultisigOut(transaction.outputs),
                multisigIn: util.MultisigIn(transaction.inputs)
            }, txValue: util.getTransactionValue(transaction.inputs, transaction.outputs, transaction.outputSatoshis)
        });
        // collectionName.update(
        //     {hash: transaction.hash},
        //     {
        //         $set: {
        //             hash: transaction.hash,
        //             date: new Date(),
        //             txValue: util.getTransactionValue(transaction.inputs, transaction.outputs, transaction.outputSatoshis),
        //             txStaticInfo: {
        //                 inputs: transaction.inputs,
        //                 valueIn: transaction.inputSatoshis,
        //                 outputs: transaction.outputs,
        //                 valueOut: transaction.outputSatoshis,
        //                 fees: transaction.feeSatoshis,
        //                 size: transaction.hex.length / 2,
        //                 fee_size_ratio: transaction.feeSatoshis / (transaction.hex.length / 2),
        //                 fee_totalInputValue_ratio: transaction.feeSatoshis / transaction.inputSatoshis,
        //                 time: timeStamp.getTimeInventory(),
        //                 location: "LND",
        //                 OP_RETURN: util.OpReturn(transaction.outputs),
        //                 multisigOut: util.MultisigOut(transaction.outputs),
        //                 multisigIn: util.MultisigIn(transaction.inputs)
        //             }
        //         }
        //     },
        //     { upsert: true}
        //     , function (err) {
        //         if (err) throw  err;
        //         callback(null, transaction.hash);
        //     });
    }
    catch (e) {
        console.error("staticInfo Err:", e);
    }

};
//process Three, updates block Statistics from monitoring Dbs to main collection..
var blockStatistic = function (hash, callback) {
    try {
        blockDetails.get('BlockDetails', function (err, lastBlocksDetail) {
            if (lastBlocksDetail) {
                var details = JSON.parse(lastBlocksDetail);
                callback(err, {
                    lastBlocksAverage30: {
                        lastBlockNumber: details[0].blockNumber,
                        median: details[0].median,
                        stdev: details[0].stdev,
                        average: details[0].average,
                        min: details[0].min,
                        quartile1: details[0].quartile1,
                        quartile3: details[0].quartile3,
                        blockSize: details[0].blockSize,
                        blockTimeGap: details[0].blockTimeGap,
                        txBlockTimeGap: Number(((timeStamp.getTimeStamp() - details[0].blockTime / 1000) / 60).toFixed())
                    },
                    lastBlocksAverage6: {
                        lastBlockNumber: details[1].blockNumber,
                        median: details[1].median,
                        stdev: details[1].stdev,
                        average: details[1].average,
                        min: details[1].min,
                        quartile1: details[1].quartile1,
                        quartile3: details[1].quartile3,
                        blockSize: details[1].blockSize,
                        blockTimeGap: details[1].blockTimeGap,
                        txBlockTimeGap: Number(((timeStamp.getTimeStamp() - details[1].blockTime / 1000) / 60).toFixed())
                    },
                    lastBlocksAverage: {
                        lastBlockNumber: details[2].blockNumber,
                        median: details[2].median,
                        stdev: details[2].stdev,
                        average: details[2].average,
                        min: details[2].min,
                        quartile1: details[2].quartile1,
                        quartile3: details[2].quartile3,
                        lastBlockSize: details[2].blockSize,
                        blockTimeGap: details[2].blockTimeGap,
                        txBlockTimeGap: Number(((timeStamp.getTimeStamp() - details[2].blockTime / 1000) / 60).toFixed())
                    },
                    previousLastBlock: {
                        lastBlockNumber: details[3].blockNumber,
                        median: details[3].median,
                        stdev: details[3].stdev,
                        average: details[3].average,
                        min: details[3].min,
                        quartile1: details[3].quartile1,
                        quartile3: details[3].quartile3,
                        lastBlockSize: details[3].blockSize,
                        blockTimeGap: details[3].blockTimeGap,
                        txBlockTimeGap: Number(((timeStamp.getTimeStamp() - details[3].blockTime / 1000) / 60).toFixed())
                    },
                    previousSecondBlock: {
                        lastBlockNumber: details[4].blockNumber,
                        median: details[4].median,
                        stdev: details[4].stdev,
                        average: details[4].average,
                        min: details[4].min,
                        quartile1: details[4].quartile1,
                        quartile3: details[4].quartile3,
                        lastBlockSize: details[4].blockSize,
                        blockTimeGap: details[4].blockTimeGap,
                        txBlockTimeGap: Number(((timeStamp.getTimeStamp() - details[4].blockTime / 1000) / 60).toFixed())
                    }
                });
            }
        })

        // collectionNameTwo.find().limit(5).sort({
        //     lastBlocksAverage: -1,
        //     _id: -1
        // }).toArray(function (err, details) {
        //     if (err) throw err;
        //     try {
        //         callback(null, {
        //             lastBlocksAverage30: {
        //                 lastBlockNumber: details[0].blockNumber,
        //                 median: details[0].median,
        //                 stdev: details[0].stdev,
        //                 average: details[0].average,
        //                 min: details[0].min,
        //                 quartile1: details[0].quartile1,
        //                 quartile3: details[0].quartile3,
        //                 blockSize: details[0].blockSize,
        //                 blockTimeGap: details[0].blockTimeGap,
        //                 txBlockTimeGap: Number(((timeStamp.getTimeStamp() - details[0].blockTime / 1000) / 60).toFixed())
        //             },
        //             lastBlocksAverage6: {
        //                 lastBlockNumber: details[1].blockNumber,
        //                 median: details[1].median,
        //                 stdev: details[1].stdev,
        //                 average: details[1].average,
        //                 min: details[1].min,
        //                 quartile1: details[1].quartile1,
        //                 quartile3: details[1].quartile3,
        //                 blockSize: details[1].blockSize,
        //                 blockTimeGap: details[1].blockTimeGap,
        //                 txBlockTimeGap: Number(((timeStamp.getTimeStamp() - details[1].blockTime / 1000) / 60).toFixed())
        //             },
        //             lastBlocksAverage: {
        //                 lastBlockNumber: details[2].blockNumber,
        //                 median: details[2].median,
        //                 stdev: details[2].stdev,
        //                 average: details[2].average,
        //                 min: details[2].min,
        //                 quartile1: details[2].quartile1,
        //                 quartile3: details[2].quartile3,
        //                 lastBlockSize: details[2].blockSize,
        //                 blockTimeGap: details[2].blockTimeGap,
        //                 txBlockTimeGap: Number(((timeStamp.getTimeStamp() - details[2].blockTime / 1000) / 60).toFixed())
        //             },
        //             previousBlock: {
        //                 lastBlockNumber: details[3].blockNumber,
        //                 median: details[3].median,
        //                 stdev: details[3].stdev,
        //                 average: details[3].average,
        //                 min: details[3].min,
        //                 quartile1: details[3].quartile1,
        //                 quartile3: details[3].quartile3,
        //                 lastBlockSize: details[3].blockSize,
        //                 blockTimeGap: details[3].blockTimeGap,
        //                 txBlockTimeGap: Number(((timeStamp.getTimeStamp() - details[3].blockTime / 1000) / 60).toFixed())
        //             },
        //             previousSecondBlock: {
        //                 lastBlockNumber: details[4].blockNumber,
        //                 median: details[4].median,
        //                 stdev: details[4].stdev,
        //                 average: details[4].average,
        //                 min: details[4].min,
        //                 quartile1: details[4].quartile1,
        //                 quartile3: details[4].quartile3,
        //                 lastBlockSize: details[4].blockSize,
        //                 blockTimeGap: details[4].blockTimeGap,
        //                 txBlockTimeGap: Number(((timeStamp.getTimeStamp() - details[4].blockTime / 1000) / 60).toFixed())
        //             }
        //         });
        //
        //         // collectionNameOne.update(
        //         //     {hash: hash},
        //         //     {
        //         //         $set: {
        //         //             lastBlocksAverage30: {
        //         //                 lastBlockNumber: details[0].blockNumber,
        //         //                 median: details[0].median,
        //         //                 stdev: details[0].stdev,
        //         //                 average: details[0].average,
        //         //                 min: details[0].min,
        //         //                 quartile1: details[0].quartile1,
        //         //                 quartile3: details[0].quartile3,
        //         //                 blockSize: details[0].blockSize,
        //         //                 blockTimeGap: details[0].blockTimeGap,
        //         //                 txBlockTimeGap: Number(((timeStamp.getTimeStamp() - details[0].blockTime / 1000) / 60).toFixed())
        //         //             },
        //         //             lastBlocksAverage6: {
        //         //                 lastBlockNumber: details[1].blockNumber,
        //         //                 median: details[1].median,
        //         //                 stdev: details[1].stdev,
        //         //                 average: details[1].average,
        //         //                 min: details[1].min,
        //         //                 quartile1: details[1].quartile1,
        //         //                 quartile3: details[1].quartile3,
        //         //                 blockSize: details[1].blockSize,
        //         //                 blockTimeGap: details[1].blockTimeGap,
        //         //                 txBlockTimeGap: Number(((timeStamp.getTimeStamp() - details[1].blockTime / 1000) / 60).toFixed())
        //         //             },
        //         //             lastBlocksAverage: {
        //         //                 lastBlockNumber: details[2].blockNumber,
        //         //                 median: details[2].median,
        //         //                 stdev: details[2].stdev,
        //         //                 average: details[2].average,
        //         //                 min: details[2].min,
        //         //                 quartile1: details[2].quartile1,
        //         //                 quartile3: details[2].quartile3,
        //         //                 lastBlockSize: details[2].blockSize,
        //         //                 blockTimeGap: details[2].blockTimeGap,
        //         //                 txBlockTimeGap: Number(((timeStamp.getTimeStamp() - details[2].blockTime / 1000) / 60).toFixed())
        //         //             }
        //         //         }
        //         //     },
        //         //     {
        //         //         upsert: true
        //         //     }
        //         //     , function (err) {
        //         //         if (err) throw err;
        //         //         callback(null, hash);
        //         //     })
        //     }
        //     catch (e) {
        //         console.error("blockStatistic Err:", e);
        //     }
        //
        // })
    }
    catch (e) {
        console.log("blockDetails:", e);
    }

};
//process For, updates confirmation list for every input txId, not use insight_api..
var confirmationList = function (node, transaction, callback) {
    var confirmations = [];
    async.eachSeries(transaction.inputs, function (input, cb) {
        try {
            node.services.bitcoind.getDetailedTransaction(input.prevTxId, function (err, trx) {
                if (err) throw err;
                var txHeight = 0;
                if (trx) {
                    if (trx.height + 1 > 0) {
                        txHeight = (node.services.bitcoind.height - trx.height) + 1;
                    }
                    confirmations.push({hash: trx.hash, confirmation: txHeight, sequence: input.sequence});

                    cb();
                }
                else {
                    confirmations.push({hash: input.prevTxId,  confirmation: txHeight, sequence: input.sequence});
                    cb();
                }
            });
        }
        catch (e) {
            console.log("getDetailedTransaction Err:", e);
            confirmations.push({hash: input.prevTxId, confirmation: null})
        }
        // async.waterfall([
        //         function (next) {
        //             request.get(config.insightApiPath + input.prevTxId, function (error, response, body) {
        //                 if (!error && response.statusCode == 200) {
        //                     var json = JSON.parse(body);
        //                     next(null, {
        //                         txid: json.txid,
        //                         confirmation: json.confirmations
        //                     });
        //                 } else {
        //                     next(null, err);
        //                 }
        //             });
        //         },
        //         function (confirmationList, next) {
        //             if (confirmationList == null) {
        //                 request.get(config.insightApiPath + input.prevTxId, function (error, response, body) {
        //                     if (!error && response.statusCode == 200) {
        //                         var json = JSON.parse(body);
        //                         //console.log("InsightApi2:", json.confirmations)
        //                         next(null, {
        //                             txid: json.txid,
        //                             confirmation: json.confirmations
        //                         });
        //                     } else {
        //                         next(null, err);
        //                     }
        //                 });
        //
        //             } else {
        //                 next(null, confirmationList);
        //             }
        //         }
        //     ],
        //     function (err, result) {
        //         if (err) cb(err);
        //         //console.log("Result", result);
        //         confirmations.push(result);
        //         cb();
        //     })
    }, function (err) {
        if (err) callback(err);
        try {
            callback(null, confirmations);
            // collectionName.update(
            //     {hash: transaction.hash},
            //     {
            //         $set: {confirmationList: confirmations}
            //     },
            //     {
            //         upsert: true
            //     }
            //     , function (err) {
            //         if (err) throw  err;
            //         callback(null, transaction.hash);
            //     })
        }
        catch (e) {
            console.error("confirmation Err:", e);
        }

    })
};
//last process
var updateAllData = function () {
    if (arguments) {
        var mainCol = arguments[0],
            dataForUpdate = arguments[1],
            callback = arguments[2];
        try {
            mainCol.update(
                {hash: dataForUpdate.hash},
                {
                    $set: dataForUpdate
                },
                {upsert: true}, function (err) {
                    if (err) throw err;
                    callback(null, dataForUpdate.hash);
                })
        }
        catch (e) {
            callback(e);
        }
    }

}


