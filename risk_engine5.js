"use strict"
var redis = require("redis");
var config = require("./libs/config");
var timeStamp = require('./utils/timeStamp');
var client = redis.createClient(config.redisConf);
var MongoClient = require('mongodb').MongoClient;
MongoClient.connect(config.monitoringDB, function (err, db) {
    if (err) throw  err;
    var blockDetails = db.collection(config.blockDetails);
    setInterval(function(){
        blockDetails.find().limit(5).sort({
            lastBlocksAverage: -1,
            _id: -1
        }).toArray(function (err, details) {
            if (err) throw err;
            client.set('BlockDetails', JSON.stringify(details), function (err) {
                if (err) throw err;
                client.get('BlockDetails',function (err, lastBlocksDetails) {
                    if(err) throw err;
                    var details = JSON.parse(lastBlocksDetails);
                    console.log(details);
                    console.log("***End***")
                })
            })
            // client.ltrim('myList', 1, -1);
            // client.rpush('myList', JSON.stringify(details), function (err, doc) {
            //     if (err) throw err;
            //     console.log(doc);
            //     client.lrange('myList', 0, -1, function (err, lastBlocksDetails) {
            //         var details = JSON.parse(lastBlocksDetails);
            //         var lastBlocksObject = {
            //             lastBlocksAverage30: {
            //                 lastBlockNumber: details[0].blockNumber,
            //                 median: details[0].median,
            //                 stdev: details[0].stdev,
            //                 average: details[0].average,
            //                 min: details[0].min,
            //                 quartile1: details[0].quartile1,
            //                 quartile3: details[0].quartile3,
            //                 blockSize: details[0].blockSize,
            //                 blockTimeGap: details[0].blockTimeGap,
            //                 txBlockTimeGap: Number(((timeStamp.getTimeStamp() - details[0].blockTime / 1000) / 60).toFixed())
            //             },
            //             lastBlocksAverage6: {
            //                 lastBlockNumber: details[1].blockNumber,
            //                 median: details[1].median,
            //                 stdev: details[1].stdev,
            //                 average: details[1].average,
            //                 min: details[1].min,
            //                 quartile1: details[1].quartile1,
            //                 quartile3: details[1].quartile3,
            //                 blockSize: details[1].blockSize,
            //                 blockTimeGap: details[1].blockTimeGap,
            //                 txBlockTimeGap: Number(((timeStamp.getTimeStamp() - details[1].blockTime / 1000) / 60).toFixed())
            //             },
            //             lastBlocksAverage: {
            //                 lastBlockNumber: details[2].blockNumber,
            //                 median: details[2].median,
            //                 stdev: details[2].stdev,
            //                 average: details[2].average,
            //                 min: details[2].min,
            //                 quartile1: details[2].quartile1,
            //                 quartile3: details[2].quartile3,
            //                 lastBlockSize: details[2].blockSize,
            //                 blockTimeGap: details[2].blockTimeGap,
            //                 txBlockTimeGap: Number(((timeStamp.getTimeStamp() - details[2].blockTime / 1000) / 60).toFixed())
            //             },
            //             previousBlock: {
            //                 lastBlockNumber: details[3].blockNumber,
            //                 median: details[3].median,
            //                 stdev: details[3].stdev,
            //                 average: details[3].average,
            //                 min: details[3].min,
            //                 quartile1: details[3].quartile1,
            //                 quartile3: details[3].quartile3,
            //                 lastBlockSize: details[3].blockSize,
            //                 blockTimeGap: details[3].blockTimeGap,
            //                 txBlockTimeGap: Number(((timeStamp.getTimeStamp() - details[3].blockTime / 1000) / 60).toFixed())
            //             },
            //             previousSecondBlock: {
            //                 lastBlockNumber: details[4].blockNumber,
            //                 median: details[4].median,
            //                 stdev: details[4].stdev,
            //                 average: details[4].average,
            //                 min: details[4].min,
            //                 quartile1: details[4].quartile1,
            //                 quartile3: details[4].quartile3,
            //                 lastBlockSize: details[4].blockSize,
            //                 blockTimeGap: details[4].blockTimeGap,
            //                 txBlockTimeGap: Number(((timeStamp.getTimeStamp() - details[4].blockTime / 1000) / 60).toFixed())
            //             }
            //         };
            //         console.log(lastBlocksObject);
            //     });
            // });
        })
    },10000)
})

//console.log(client);
// var async = require('async');
// var request = require('request');
// var config = require('./libs/config');
// console.log(config.blockTrailPath + "00000000000000000f0487597dfa4cfc518fce451d1ee9de853b6eafc58366f"+ config.blockTrailKey)
// async.waterfall([
//     function (cb) {
//         try {
//             request.get(config.blockTrailPath + "000000000000000000f0487597dfa4cfc518fce451d1ee9de853b6eafc58366" + config.blockTrailKey, function (error, response, body) {
//                 if (!error && response.statusCode == 200) {
//                     var json = JSON.parse(body);
//                     cb(null, json.data);
//                 }
//                 else {
//                     cb(null, null);
//                 }
//             })
//         }
//         catch (e) {
//             cb(e)
//         }
//
//     },
//     function (txList, cb) {
//         if (txList == null) {
//             request.get({
//                 url: "https://blockchain.info/rawblock/000000000000000000f0487597dfa4cfc518fce451d1ee9de853b6eafc58366f",
//                 strictSSL: false
//             }, function (error, response, body) {
//                 if (!error && response.statusCode == 200) {
//                     var json = JSON.parse(body);
//                     cb(null, json.tx);
//
//                 }
//                 else {
//                     cb(null, null);
//                 }
//             })
//         }
//         else {
//             cb(null, txList)
//         }
//     }
// ], function (err, txList) {
//     console.log("Status:", txList);
//
// })
//
// var updateBlockDetails = function(collectionName, data){
//     console.log(data.transactions[0].toObject());
//     var fee, size, numbers = [],
//         transactions = data.transactions,
//         blockDetails;
//     collectionName.find(
//         {blockHash: data.previousBlock},
//         {blockTime: true}
//     ).toArray(function (err, prevBlock) {
//         var timeGap;
//
//         if(!err && prevBlock.length)
//             timeGap = Number(( (data.blockTime - prevBlock[0].blockTime / 1000) / 60).toFixed(2));
//         else
//             timeGap = 0;
//
//         for(var i in transactions){
//
//             fee = transactions[i].total_fee ? transactions[i].total_fee : getTransactionFee(transactions[i].inputs, transactions[i].out);
//             //console.log(fee);
//             size = transactions[i].size;
//
//             if(fee > 0 && size > 0)
//                 numbers.push(fee/size);
//         }
//
//         if(numbers.length)
//             blockDetails = {
//                 blockHash: data.hash,
//                 blockNumber: data.height,
//                 blockTime: data.blockTime * 1000,
//                 blockSize: data.size,
//                 blockTimeGap: timeGap,
//                 median : stats.median(numbers),
//                 stdev : stats.stdev(numbers),
//                 average : stats.mean(numbers),
//                 min : Math.min.apply(null, numbers),
//                 quartile1: getQuartile1(numbers),
//                 quartile3: getQuartile3(numbers)
//             };
//         else
//             blockDetails = null;
//
//         if (collectionName && blockDetails) {
//
//             collectionName.save(blockDetails, function (err, data) {
//                 var i = 30;
//
//                 if(data)
//                     collectionName.find({lastBlocksAverage: {$exists: false}}).sort({_id: -1}).limit(i).toArray(function (err, data) {
//
//                         if(data.length) {
//                             collectionName.update(
//                                 {lastBlocksAverage: i},
//                                 {$set: getBlockAverages(data, i, blockDetails)},
//                                 {upsert: true}
//                             );
//                             collectionName.update(
//                                 {lastBlocksAverage: 6},
//                                 {$set: getBlockAverages(data, 6, blockDetails)},
//                                 {upsert: true}
//                             )
//                         }
//                     })
//             });
//         }
//     })
// };

//  var bitcore = require('bitcore-node');
// var lib = require('bitcore-lib');
// var MongoClient = require('mongodb').MongoClient;
// var assert = require('assert');
// var async = require('async');
// var config = require('./libs/config');
// var util = require('./utils/utilBitcore');
// var time = require('./utils/time');
// var timeStamp = require('./utils/timeStamp');
// var BufferUtil = lib.util.buffer;
//
// //Services
// var Bitcoin = bitcore.services.Bitcoin;
//
// var configuration = {
//     network: 'livenet',
//     services: [
//         {
//             name: 'bitcoind',
//             module: Bitcoin,
//             config: {
//                 spawn: {
//                     "datadir": "/root/.bitcore/data",
//                     "exec": "/root/.nvm/versions/node/v0.12.10/lib/node_modules/bitcore/node_modules/bitcore-node/bin/bitcoind"
//                 }
//             }
//         }
//     ]
// }
// var node = new bitcore.Node(configuration);
// var bus = node.openBus();
//
// node.start(function (err) {
//     if (err) {
//         assert.equal(null, err)
//         node.stop(function () {
//             console.log('shutdown the node');
//         });
//     }
// });
// node.on('error', function (err) {
//     assert.equal(null, err);
//     node.stop();
// });
//
// node.on('ready', function () {
//     async.parallel({
//             RE: function (cb) {
//                 MongoClient.connect(config.riskEngineDB, function (err, r5DB) {
//                     cb(err, r5DB);
//                 })
//             },
//             MDB: function (cb) {
//                 MongoClient.connect(config.monitoringDB, function (err, mDB) {
//                     cb(err, mDB);
//                 })
//             }
//         },
//         function (err, DBs) {
//             assert.equal(null, err);
//             console.log("Connected correctly to Dbs");
//             var blockDetails = DBs.MDB.collection(config.blockDetails);
//             var mainCol = DBs.RE.collection(config.mainCol);
//             mainCol.createIndex({"hash": 1}, {dropDups: true, unique: true});
//             var memPoolCol = DBs.RE.collection(config.memPoolCol);
//             memPoolCol.createIndex({"date": 1}, {expireAfterSeconds: 15});
//             memPoolCol.createIndex({"txIds.txId": 1});
//             bus.subscribe('bitcoind/rawtransaction');
//             bus.on('bitcoind/rawtransaction', function (transactionHex) {
//                 var tx = new lib.Transaction(transactionHex);
//                 node.services.bitcoind.getDetailedTransaction(tx.hash, function (err, transaction) {
//                     if (transaction != undefined) {
//                         if (transaction.coinbase == true) {
//                             try {
//                                 mainCol.insertOne(
//                                     {
//                                         hash: transaction.hash,
//                                         date: new Date(),
//                                         txValue: transaction.outputSatoshis,
//                                         coinbase: transaction.coinbase,
//                                         txStaticInfo: {
//                                             inputs: transaction.inputs
//                                             , outputs: transaction.outputs
//                                             , valueOut: transaction.outputSatoshis
//                                             , size: transaction.hex.length / 2
//                                             , time: timeStamp.getTimeInventory()
//                                             , location: "LND"
//                                         },
//
//                                         blockHash: transaction.blockHash,
//                                         blockNumber: transaction.height,
//                                         blockTime: transaction.blockTimestamp
//
//                                     });
//                             }
//                             catch (e) {
//                                 console.error("coinbase error:", e);
//                             }
//
//                         }
//                         else {
//                             var hash = transaction.hash;
//                             memPoolCol.insert({
//                                 hash: hash,
//                                 date: new Date(),
//                                 txIds: util.getForMemPoolList(transaction.inputs)
//                             });
//                             async.series({
//                                 insertOne: function (callback) {
//                                     try {
//                                         mainCol.insertOne({
//                                             hash: hash,
//                                             date: new Date()
//                                         }, function (err) {
//                                             if (err) {
//                                                 callback(err);
//                                             }
//                                             callback(null, hash);
//                                         });
//                                     }
//                                     catch (e) {
//                                         console.error("insertOne err:", e)
//                                     }
//                                 },
//                                 staticInfo: function (callback) {
//                                     try {
//                                         mainCol.update(
//                                             {hash: hash},
//                                             {
//                                                 $set: {
//                                                     txValue: util.getTransactionValue(transaction.inputs, transaction.outputs, transaction.outputSatoshis),
//                                                     txStaticInfo: {
//                                                         inputs: transaction.inputs,
//                                                         valueIn: transaction.inputSatoshis,
//                                                         outputs: transaction.outputs,
//                                                         valueOut: transaction.outputSatoshis,
//                                                         fees: transaction.feeSatoshis,
//                                                         size: transaction.hex.length / 2,
//                                                         fee_size_ratio: transaction.feeSatoshis / (transaction.hex.length / 2),
//                                                         fee_totalInputValue_ratio: transaction.feeSatoshis / transaction.inputSatoshis,
//                                                         time: timeStamp.getTimeInventory(),
//                                                         location: "LND",
//                                                         OP_RETURN: util.OpReturn(transaction.outputs),
//                                                         multisigOut: util.MultisigOut(transaction.outputs),
//                                                         multisigIn: util.MultisigIn(transaction.inputs)
//                                                     }
//                                                 },
//                                             }, { upsert: true },
//                                             function (err) {
//                                                 if (err) {
//                                                     callback(err);
//                                                 }
//                                                 callback(null, hash);
//                                             });
//                                     }
//                                     catch (e) {
//                                         console.error("txStaticInfo", e);
//                                     }
//                                 },
//                                 txConfirm: function (callback) {
//                                     var confirmations = [];
//                                     async.eachSeries(transaction.inputs, function (input, cb) {
//                                         try {
//                                             node.services.bitcoind.getDetailedTransaction(input.prevTxId, function (err, trx) {
//                                                 var txHeight = 0;
//                                                 if (trx) {
//                                                     if (trx.height + 1 > 0) {
//                                                         txHeight = (node.services.bitcoind.height - trx.height) + 1;
//                                                     }
//                                                     confirmations.push({hash: trx.hash, confirmation: txHeight});
//
//                                                     cb();
//                                                 }
//                                             });
//                                         }
//                                         catch (e) {
//                                             console.log("getDetailedTransaction Err:", e);
//                                             confirmations.push({hash: input.prevTxId, confirmation: 'undifind'})
//                                         }
//
//                                     }, function (err) {
//                                         if (err) {
//                                             callback(err);
//                                         }
//                                         try {
//                                             mainCol.update(
//                                                 {hash: hash},
//                                                 {
//                                                     $set: {confirmationList: confirmations}
//                                                 },{ upsert: true},
//                                                 function (err) {
//                                                     if (err) callback(err);
//                                                     callback(null, hash);
//                                                 })
//                                         }
//                                         catch (e) {
//                                             console.error("confirmation Err:", e);
//                                         }
//
//                                     })
//                                 },
//                                 blockDetail: function (callback) {
//                                     blockDetails.find().limit(3).sort({
//                                         lastBlocksAverage: -1,
//                                         _id: -1
//                                     }).toArray(function (err, details) {
//                                         if (err) {
//                                             callback(err);
//                                         }
//                                         try {
//                                             mainCol.update(
//                                                 {hash: hash},
//                                                 {
//                                                     $set: {
//                                                         lastBlocksAverage30: {
//                                                             lastBlockNumber: details[0].blockNumber,
//                                                             median: details[0].median,
//                                                             stdev: details[0].stdev,
//                                                             average: details[0].average,
//                                                             min: details[0].min,
//                                                             quartile1: details[0].quartile1,
//                                                             quartile3: details[0].quartile3,
//                                                             blockSize: details[0].blockSize,
//                                                             blockTimeGap: details[0].blockTimeGap,
//                                                             txBlockTimeGap: Number(((timeStamp.getTimeStamp() - details[0].blockTime / 1000) / 60).toFixed())
//                                                         },
//                                                         lastBlocksAverage6: {
//                                                             lastBlockNumber: details[1].blockNumber,
//                                                             median: details[1].median,
//                                                             stdev: details[1].stdev,
//                                                             average: details[1].average,
//                                                             min: details[1].min,
//                                                             quartile1: details[1].quartile1,
//                                                             quartile3: details[1].quartile3,
//                                                             blockSize: details[1].blockSize,
//                                                             blockTimeGap: details[1].blockTimeGap,
//                                                             txBlockTimeGap: Number(((timeStamp.getTimeStamp() - details[1].blockTime / 1000) / 60).toFixed())
//                                                         },
//                                                         lastBlocksAverage: {
//                                                             lastBlockNumber: details[2].blockNumber,
//                                                             median: details[2].median,
//                                                             stdev: details[2].stdev,
//                                                             average: details[2].average,
//                                                             min: details[2].min,
//                                                             quartile1: details[2].quartile1,
//                                                             quartile3: details[2].quartile3,
//                                                             lastBlockSize: details[2].blockSize,
//                                                             blockTimeGap: details[2].blockTimeGap,
//                                                             txBlockTimeGap: Number(((timeStamp.getTimeStamp() - details[2].blockTime / 1000) / 60).toFixed())
//                                                         }
//                                                     }
//                                                 }, { upsert: true },function (err) {
//                                                     if (err) callback(err);
//                                                     callback(null, hash);
//                                                 })
//                                         }
//                                         catch (e) {
//                                             console.error("lastBlocksAverage Err:", e);
//                                         }
//
//                                     })
//                                 }
//                             }, function (err, results) {
//                                 //assert.equal(null, err);
//                                 if(err){
//                                     console.log(err);
//                                 }
//                                 //console.log(results.insertOne);
//                                 mainCol.update({
//                                         hash: results.staticInfo || results.txConfirm || results.blockDetail
//                                     },
//                                     {$set: {endDate: new Date()}});
//
//                             })
//
//                         }
//                     }
//
//                 })
//
//             });
//
//             bus.subscribe('bitcoind/hashblock');
//             bus.on('bitcoind/hashblock', function (blockhashHex) {
//                 console.log("BlockHex:", blockhashHex);
//                 setTimeout(function () {
//                     node.services.bitcoind.getBlockHeader(blockhashHex, function (err, blockHeader) {
//                         if (err) {
//                             console.error(err);
//                         }
//                         console.log('blockHeader:', blockHeader);
//                         node.services.bitcoind.getBlock(blockHeader.hash, function (err, block) {
//                             if (err) {
//                                 console.log(err);
//                             }
//                             console.log("block.hash:", block.hash);
//                             console.log("last block Length", block.transactions.length);
//                             block.transactions.forEach(function (transaction) {
//                                 try {
//                                     mainCol.updateOne(
//                                         {
//                                             "hash": transaction.hash
//                                         },
//                                         {
//                                             $set: {
//                                                 "blockHash": block.hash,
//                                                 "previousBlock": blockHeader.prevHash,
//                                                 "blockTime": block.header.time,
//                                                 "blockNumber": blockHeader.height
//                                             }
//                                         }, {upsert: true}
//                                     )
//
//                                 }
//                                 catch (e) {
//                                     console.log("update BlockHash Err:", e);
//                                 }
//                             })
//
//                         });
//                         node.services.bitcoind.getBlock(blockHeader.prevHash, function (err, block) {
//                             if (err) {
//                                 console.log(err);
//                             }
//                             console.log("prevHash:", block.hash);
//                             console.log("prev block Length", block.transactions.length);
//                             block.transactions.forEach(function (transaction) {
//                                 try {
//                                     mainCol.updateOne(
//                                         {
//                                             "hash": transaction.hash
//                                         },
//                                         {
//                                             $set: {
//                                                 "blockHash": block.hash,
//                                                 "previousBlock": blockHeader.prevHash,
//                                                 "blockTime": block.header.time,
//                                                 "blockNumber": blockHeader.height
//                                             }
//                                         }, {upsert: true}
//                                     )
//
//                                 }
//                                 catch (e) {
//                                     console.log("update BlockHash Err:", e);
//                                 }
//                             })
//
//                         });
//                     })
//
//
//
//                 }, 2000)
//             });
//
//         })
// });

