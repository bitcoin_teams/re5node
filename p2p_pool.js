"use strict"
var config = require('./libs/config');
var p2p = require('bitcore-p2p');
var lib = require('bitcore-lib');
var async = require('async');
var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var Pool = p2p.Pool;
var Networks = lib.Networks;
var Messages = p2p.Messages;
var messages = new Messages();
var redis = require("redis");
var redisClient = redis.createClient();
var pool = new Pool({
    network: Networks.livenet
});
var connectionOptions = {
    db: {bufferMaxEntries: 0},
    server: {
        reconnectTries: 30000,
        reconnectInterval: 1000,
        auto_reconnect: true
    }
};

/*
 * Add new 'getDiff' function to Date class,
 * which will calculate and return difference between dates by given unit.
 * Unit value can be 'd' - day, 's' - second.
 * */
Date.prototype.getDiff = function (unit, date) {
    var diff;
    date = date || new Date();

    if (unit == 'd')
        diff = 24 * 60 * 60 * 1000;

    if (unit == 's')
        diff = 1000;

    return ((date.getTime() - this.getTime()) / diff).toFixed(2);
};

pool.connect();

async.parallel({
        RDB: function (cb) {
            MongoClient.connect(config.riskEngineDB, connectionOptions,
                function (err, r1DB) {
                    cb(err, r1DB);
                })
        },
        PDB: function (cb) {
            MongoClient.connect(config.memPoolDB, connectionOptions,
                function (err, PDB) {
                    cb(err, PDB);
                })
        },
        MDB: function (cb) {
            MongoClient.connect(config.monitoringDB, connectionOptions,
                function (err, MDB) {
                    cb(err, MDB);
                })
        }
    },
    function (err, DBs) {
        assert.equal(null, err);
        var timestamp = new Date();
        console.log("DBs connected.. " + timestamp);

        var mainCol = DBs.RDB.collection(config.mainCol);
        var doubleSpends = DBs.MDB.collection(config.doubleSpends);
        var p2pCol = DBs.PDB.collection(config.p2pCol);

        doubleSpends.createIndex({hash: 1, riskEngineNumber: 1}, {unique: true});

        pool.on('peerconnect', function (peer) {

            peer.on('tx', onTransaction);
            peer.on('inv', onInv);

            peer.on('addr', function (message) {
                if (pool.numberConnected() >= 800)
                    return;

                message.addresses.forEach(function (addr) {
                    pool._connectPeer(addr);
                });
            });
        });


        function onInv(message) {
            try {
                var Invs = message.inventory,
                    peer = this;

                async.eachSeries(Invs, function (inv, cb) {
                    var invHash = lib.util.buffer.bufferToHex(lib.util.buffer.reverse(inv.hash));

                    redisClient.set([invHash + "-1s", invHash, 'EX', 1, 'NX'], function (err, res) {
                        if (err)
                            return console.error("redis error ", err);

                        if (res)
                            redisClient.get(invHash, function (err, doc) {
                                if (err) {
                                    console.log(err, invHash + " 3333  " + new Date());
                                    return cb(err);
                                }

                                if (doc == null) {
                                    //console.log(new Date(), invHash + "  find");
                                    peer.sendMessage(messages.GetData.forTransaction(invHash));

                                    cb();
                                } else {
                                    cb();
                                }
                            });
                    });

                    if (new Date() - timestamp > 30 * 1000) {
                        p2pCol.insert({
                                hash: invHash,
                                date: new Date(),
                                numberConnected: pool.numberConnected()
                            },
                            {writeConcern: {w: "majority", wtimeout: 3 * 1000}},
                            function (err) {
                                if (err)
                                    console.log(err, "errorr 1111");

                                console.log(new Date(), invHash + '   insert 2222');

                                redisClient.dbsize(function (err, count) {
                                    console.log("REDIS -> " + count + " CONS -> " + pool.numberConnected());
                                });

                            });

                        timestamp = new Date();
                    }

                }, function (err) {
                    assert.equal(null, err);
                });

            } catch (err) {
                console.log("err1 -- ", err)
            }
        }

        function onTransaction(message) {
            var Inputs = message.transaction.toObject().inputs;
            var invHash = message.transaction.toObject().hash;
            var Outputs = message.transaction.toObject().outputs;
            var harmles = true;
            async.forEachSeries(Inputs, function (input, cb) {

                redisClient.get(input.prevTxId + "#" + input.outputIndex, function (err, doubleSpent) {
                    if (err) {
                        console.log(err, invHash + " 4444  " + new Date());
                        return cb(err);
                    }

                    var doubleSpentJson = JSON.parse(doubleSpent)

                    if (doubleSpent == null)
                        return cb();

                    if (doubleSpentJson.hash == invHash)
                        return cb();


                    Outputs.forEach(function(invOutput){
                        doubleSpentJson.outputs.forEach(function(output){
                            if(invOutput.satoshis != output.satoshis || invOutput.script != output.script){
                                console.log(" No HarmlessDoubleSpend");
                                console.log("invOutputs:", Outputs)
                                console.log("********txOutputs:", doubleSpentJson.outputs);
                                harmles = false
                            }
                        })
                    })

                    var date = new Date();

                    async.waterfall([
                        function (callback) {
                            mainCol.findOne(
                                {
                                    hash: doubleSpentJson.hash,
                                    doubleSpent: {$exists: false}
                                },
                                {
                                    riskScore: true,
                                    date: true
                                },
                                function (err, trx) {
                                    if (err)
                                        return callback(err);

                                    if (trx)
                                        trx.timeDiff = new Date(trx.date).getDiff('s', date);

                                    callback(null, trx);
                                }
                            )
                        },
                        function (trx, callback) {
                            if (!trx)
                                return callback(null, trx);

                            mainCol.update(
                                {
                                    hash: doubleSpentJson.hash,
                                    doubleSpent: {$exists: false}
                                },
                                {
                                    $currentDate: {
                                        doubleSpentModified: true
                                    },
                                    $set: {
                                        doubleSpent: true,
                                        doubleSpentTxID: invHash,
                                        harmlessDoubleSpend: harmles,
                                        doubleSpentTime: date,
                                        doubleSpentTimeDiff: trx.timeDiff,
                                        numberConnected: pool.numberConnected()
                                    }
                                },
                                function (err) {
                                    console.log(err, doubleSpent + " transactions_re update " + trx.date + '  ' + date);

                                    if (err)
                                        return callback(err);

                                    callback(null, trx);
                                }
                            )
                        },
                        function (trx, callback) {
                            if (!trx)
                                return callback(null, trx);

                            doubleSpends.insert(
                                {
                                    hash: doubleSpentJson.hash,
                                    riskEngineNumber: 5,
                                    riskScore: trx.riskScore,
                                    harmlessDoubleSpend: harmles,
                                    doubleSpentTxID: invHash,
                                    doubleSpentTime: date,
                                    doubleSpentTimeDiff: trx.timeDiff,
                                    numberConnected: pool.numberConnected()
                                },
                                function (err) {
                                    if (
                                        err && (
                                            err.code == 11000 ||
                                            err.code == 11001
                                        )
                                    ) {
                                        console.log(err, invHash + " doubleSpends insert " + new Date());

                                        return callback();
                                    }
                                    else if (err)
                                        return callback(err);

                                    callback(null, doubleSpent)
                                });
                        }
                    ], function (err) {
                        if (err) {
                            cb(err);
                        }

                        cb();
                    })
                })
            }, function (err) {
                assert.equal(null, err);
            })
        }
    }
);
