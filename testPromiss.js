'use strict';
var Promise = require("bluebird");
var cron = require('node-cron');
var config = require('./libs/config');
var MongoClient = require('mongodb').MongoClient
    , assert = require('assert');
var delay = function (db) {
    var test = db.collection('transactions_re1');
    return new Promise(function(resolve, reject) {
        test.find({}).limit(10).toArray(function (err, doc) {
            if (err) reject(err);
            console.log('doc:', doc.length);
            doc.forEach(function (i) {
                console.log(i)
                resolve();
            })

        })
    })
};

MongoClient.connect(config.riskEngineOneDB, {
    db: {bufferMaxEntries: 0},
    server: {
        reconnectTries: 30000,
        reconnectInterval: 1000,
        auto_reconnect: true
    }
}, function (err, db) {
    assert.equal(err, null);
    delay(db).then(function () {
        console.log("Do something next");
    });
})
